WHAT IS Black Blog?
------------------------

Black Blog is a professional theme with a div based, fixed width, 2 column layout.

FEATURES
--------

 * Black Blog has an in build slideshow feature

 * To enable/configure slideshow do the following steps
   
   * Goto appearance/settings/black_blog.
   * Select Banner management and upload the slideshow images and click 'Save configuration'.
 
 * Black Blog has an in style switcher function

 * Style Switcher can be enabled only by admin. To enable/configure style switcher do the following steps

   * Goto appearance/settings/black_blog.
   * You can find dropdown menu 'Select Color'. Select required color from the list and click 'Save configuration'.
 

INSTALLATION
------------

 1. Download Black Blog from http://drupal.org/project/black_blog

 2. Unpack the downloaded files, take the folders and place them in your
    Drupal installation under one of the following locations:
      sites/all/themes
        making it available to the default Drupal site and to all Drupal sites
        in a multi-site configuration
      sites/default/themes
        making it available to only the default Drupal site
      sites/example.com/themes
        making it available to only the example.com site if there is a
        sites/example.com/settings.php configuration file

    Note: you will need to create the "themes" folder under "sites/all/"
    or "sites/default/".

FURTHER READING
---------------

Full documentation on using Black Blog:
  http://www.freedrupalthemes.net/docs/black_blog

Drupal theming documentation in the Theme Guide:
  http://drupal.org/theme-guide
  
Black Lagoon demo site
  http://d7.freedrupalthemes.net/t/black_blog
